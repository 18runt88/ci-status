var app = angular.module('app', ['ui.bootstrap', 'yaru22.angular-timeago']);

app.controller('DashboardController', function($scope, $http) {
    $scope.repositories = window.repositories;

    $scope.switchBranch = function (branch) {
        if (!branch) {
            return;
        }
        $scope.branch = branch;
        $scope.formBranch = $scope.branch;
        $scope.counter = {
            builds: 0,
            failing: 0
        };
        angular.forEach($scope.repositories, $scope.refreshRepository);
    };

    $scope.refreshRepository = function(repository) {
        repository.build = null;
        repository.order = null;

        var endpoint = repository.isPro ? 'https://api.travis-ci.com' : 'https://api.travis-ci.org';
        var request = {
            url: endpoint + '/repos/' + repository.name + '/branches/' + $scope.branch,
            headers: {
                Accept: 'application/vnd.travis-ci.2+json',
                Authorization: 'token ' + repository.travisToken
            }
        };

        $http(request).then(
            function (response) {
                repository.build = {};
                $scope.counter.builds++;
                repository.build.status = response.data.branch.state;

                switch (repository.build.status) {
                    case 'errored':
                        repository.order = 1;
                        $scope.counter.failing++;
                        break;
                    case 'failed':
                        repository.order = 2;
                        $scope.counter.failing++;
                        break;
                    case 'started':
                        repository.order = 3;
                        break;
                    case 'created':
                        repository.order = 4;
                        break;
                    case 'passed':
                        repository.order = 5;
                        break;
                    case 'canceled':
                        repository.order = 6;
                        break;
                    default:
                        console.log('Unknown build status: ' + repository.status);
                        repository.order = 7;
                        break;
                }

                var baseUrl;
                if (repository.isPro) {
                    baseUrl = 'https://magnum.travis-ci.com/' + repository.name;
                } else {
                    baseUrl = 'https://travis-ci.org/' + repository.name;
                }
                repository.build.url = baseUrl + '/builds/' + response.data.branch.id;
                repository.build.time = (!!response.data.branch.started_at) ? response.data.branch.started_at : response.data.commit.committed_at;
                if (!! response.data.branch.finished_at) {
                    // Build duration
                    var started = new Date(response.data.branch.started_at);
                    var finished = new Date(response.data.branch.finished_at);
                    repository.build.duration = Math.round((finished.getTime() - started.getTime()) / 60000);
                }

                repository.commit = {
                    url: response.data.commit.compare_url,
                    time: response.data.commit.committed_at,
                    message: response.data.commit.message,
                    author: response.data.commit.author_name
                };
            },
            function () {
                repository.build = {
                    status: null
                };
            }
        );
    };

    $scope.switchBranch('master');
});

app.filter('split', function() {
    return function(string, delimiter, resultIndex) {
        return string.split(delimiter)[resultIndex];
    }
});
app.filter('ucfirst', function() {
    return function(string) {
        return string.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
    };
});
